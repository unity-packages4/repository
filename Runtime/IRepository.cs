namespace Repository.Runtime
{
    public interface IRepository
    {
        bool Has(in string key);
        void Load<T>(in string key, in T data) where T : class;
        void Save<T>(in string key, in T data) where T : class;
    }
}