using UnityEngine;

namespace Repository.Runtime.PlayerPrefs
{
    public class PlayerPrefsRepository : IRepository
    {
        public bool Has(in string key)
        {
            return UnityEngine.PlayerPrefs.HasKey(key);
        }

        public void Load<T>(in string key, in T data) where T : class
        {
            var json =  UnityEngine.PlayerPrefs.GetString(key);
            JsonUtility.FromJsonOverwrite(json, data);
        }

        public void Save<T>(in string key, in T data) where T : class
        {
            var json = JsonUtility.ToJson(data);
            UnityEngine.PlayerPrefs.SetString(key, json);
        }
    }
}