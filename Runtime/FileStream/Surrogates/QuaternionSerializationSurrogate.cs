using System.Runtime.Serialization;
using UnityEngine;

namespace Runtime.FileStream.Surrogates
{
    internal class QuaternionSerializationSurrogate :  ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            var q = (Quaternion) obj;
            info.AddValue("x", q.x);
            info.AddValue("y", q.y);
            info.AddValue("z", q.z);
            info.AddValue("w", q.w);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            var q = new Quaternion
            {
                x = (float)info.GetValue("x", typeof(float)),
                y = (float)info.GetValue("y", typeof(float)),
                z = (float)info.GetValue("z", typeof(float)),
                w = (float)info.GetValue("w", typeof(float))
            };
            return q;
        }
    }
}