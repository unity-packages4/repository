using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Repository.Runtime.FileStream.Extensions;
using Runtime.FileStream.Surrogates;
using UnityEngine;

namespace Repository.Runtime.FileStream
{
    [Serializable]
    public class FileStreamRepository : IRepository
    {
        private const string FileSaveNameExtension = "save";
        
        public bool Has(in string key)
        {
            return File.Exists(GetPath(key));
        }

        private static string GetPath(in string key)
        {
            return $"{Application.persistentDataPath}/{key}.{FileSaveNameExtension}";
        }

        public void Save<T>(in string key, in T data) where T : class
        {
            var bf = GetFormatter();
            var file = File.Create(GetPath(key));
            bf.Serialize(file, data);
            file.Close();
        }

        private static BinaryFormatter GetFormatter()
        {
            var v3 = new Vector3SerializationSurrogate();
            var q = new QuaternionSerializationSurrogate();
            
            var selector = new SurrogateSelector();
            selector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), v3);
            selector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), q);
            
            return new BinaryFormatter
            {
                SurrogateSelector = selector
            };
        }

        public void Load<T>(in string key, in T data) where T : class
        {
            if (Has(key) == false)
            {
                return;
            }

            var path = GetPath(key);
            try
            {
                var d = LoadFile<T>(path);
                data.CopyPublicFieldAndProperties(d);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        private T LoadFile<T>(in string path)
        {
            T data;
            var file = File.Open(path, FileMode.Open);
            try
            {
                var bf = GetFormatter();
                data = (T) bf.Deserialize(file);
            }
            catch (Exception e)
            {
                File.Delete(path);
                throw e;
            }
            finally
            {
                file.Close();
            }

            return data;
        }
    }
}