using System.Reflection;

namespace Repository.Runtime.FileStream.Extensions
{
    internal static class ObjectExtension
    {
        public static void CopyPublicFieldAndProperties<T>(this T target, T source)
        {
            var type = typeof(T);
            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (var field in fields)
            {
                var value = field.GetValue(source);
                field.SetValue(target, value);
            }

            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
            {
                var value = property.GetValue(source);
                property.SetValue(target, value);
                // property.SetValue(target, value, BindingFlags.SetProperty, null, null, CultureInfo.CurrentCulture);
            }    
        }
    }
}